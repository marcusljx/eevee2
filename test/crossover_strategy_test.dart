import 'dart:core';

import 'package:eevee2/strategy/crossover.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:meta/meta.dart';

class TestCase {
  final CrossoverStrategy _strategy;

  final String a;
  final String b;
  final String expected;

  TestCase.midPointSwap({
    @required this.a,
    @required this.b,
    @required this.expected,
  }) : _strategy = CrossoverStrategies.midPointSwap();

  TestCase.nIntervalSwap({
    @required int interval,
    @required this.a,
    @required this.b,
    @required this.expected,
  }) : _strategy = CrossoverStrategies.nIntervalSwapBuilder(interval);

  run() {
    test(
      '("$a", "$b")',
      () {
        expect(
          _strategy(
            a.split(''),
            b.split(''),
          ),
          equals(
            expected.split(''),
          ),
        );
      },
    );
  }
}

void main() {
  group('midPointSwap', () {
    [
      TestCase.midPointSwap(a: "", b: "", expected: ""),
      TestCase.midPointSwap(a: "a", b: "b", expected: "a"),
      TestCase.midPointSwap(a: "aa", b: "bb", expected: "ab"),
      TestCase.midPointSwap(a: "aaa", b: "bbb", expected: "aab"),
      TestCase.midPointSwap(a: "aaaa", b: "bbbb", expected: "aabb"),
    ].forEach((testCase) {
      testCase.run();
    });
  });

  group('1-IntervalSwap', () {
    [
      TestCase.nIntervalSwap(interval: 1, a: "", b: "", expected: ""),
      TestCase.nIntervalSwap(interval: 1, a: "a", b: "b", expected: "a"),
      TestCase.nIntervalSwap(interval: 1, a: "aa", b: "bb", expected: "ab"),
      TestCase.nIntervalSwap(interval: 1, a: "aaa", b: "bbb", expected: "aba"),
      TestCase.nIntervalSwap(
          interval: 1, a: "aaaa", b: "bbbb", expected: "abab"),
      TestCase.nIntervalSwap(
          interval: 1, a: "aaaaa", b: "bbbbb", expected: "ababa"),
    ].forEach((testCase) {
      testCase.run();
    });
  });

  group('2-IntervalSwap', () {
    [
      TestCase.nIntervalSwap(interval: 2, a: "", b: "", expected: ""),
      TestCase.nIntervalSwap(interval: 2, a: "a", b: "b", expected: "a"),
      TestCase.nIntervalSwap(interval: 2, a: "aa", b: "bb", expected: "aa"),
      TestCase.nIntervalSwap(interval: 2, a: "aaa", b: "bbb", expected: "aab"),
      TestCase.nIntervalSwap(
          interval: 2, a: "aaaa", b: "bbbb", expected: "aabb"),
      TestCase.nIntervalSwap(
          interval: 2, a: "aaaaa", b: "bbbbb", expected: "aabba"),
      TestCase.nIntervalSwap(
          interval: 2, a: "aaaaaa", b: "bbbbbb", expected: "aabbaa"),
      TestCase.nIntervalSwap(
          interval: 2, a: "aaaaaaa", b: "bbbbbbb", expected: "aabbaab"),
    ].forEach((testCase) {
      testCase.run();
    });
  });

  group('3-IntervalSwap', () {
    [
      TestCase.nIntervalSwap(interval: 3, a: "", b: "", expected: ""),
      TestCase.nIntervalSwap(interval: 3, a: "a", b: "b", expected: "a"),
      TestCase.nIntervalSwap(interval: 3, a: "aa", b: "bb", expected: "aa"),
      TestCase.nIntervalSwap(interval: 3, a: "aaa", b: "bbb", expected: "aaa"),
      TestCase.nIntervalSwap(
          interval: 3, a: "aaaa", b: "bbbb", expected: "aaab"),
      TestCase.nIntervalSwap(
          interval: 3, a: "aaaaa", b: "bbbbb", expected: "aaabb"),
      TestCase.nIntervalSwap(
          interval: 3, a: "aaaaaa", b: "bbbbbb", expected: "aaabbb"),
      TestCase.nIntervalSwap(
          interval: 3, a: "aaaaaaa", b: "bbbbbbb", expected: "aaabbba"),
    ].forEach((testCase) {
      testCase.run();
    });
  });
}
