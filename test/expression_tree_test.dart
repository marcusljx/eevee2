import 'package:eevee2/ds/expression_tree/expression_tree.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:meta/meta.dart';
import 'package:tuple/tuple.dart';

class TestCase<T> {
  final String name;
  final Node<T> tree;
  final T expectedValue;
  final String expectedExpressionString;

  TestCase({
    @required this.name,
    @required this.tree,
    @required this.expectedValue,
    @required this.expectedExpressionString,
  });
}

final Node bigTree = Select.left(
  Math.negate(Bitwise.not(
    Math.add(
      ValueNode(10),
      Math.max(
        Math.multiply(
          ValueNode(4),
          ValueNode(17),
        ),
        ValueNode(201),
      ),
    ),
  )),
  Math.pow(
    ValueNode(4),
    Math.min(
      ValueNode(13),
      Math.mod(
        ValueNode(509),
        Math.pow(
          ValueNode(3),
          ValueNode(6),
        ),
      ),
    ),
  ),
);

main() {
  group('tests for readability', () {
    List<TestCase> testCases = [
      TestCase(
        name: 'value',
        tree: ValueNode(10),
        expectedValue: 10,
        expectedExpressionString: "10",
      ),
      TestCase(
        name: 'simple',
        tree: Math.negate(ValueNode(10)),
        expectedValue: -10,
        expectedExpressionString: "-10",
      ),
      TestCase(
        name: 'complex symbolic',
        tree: Math.negate(
          Math.multiply(
            Math.divide(
              ValueNode(9),
              ValueNode(6),
            ),
            ValueNode(8),
          ),
        ),
        expectedValue: -12,
        expectedExpressionString: "-((9 / 6) * 8)",
      ),
      TestCase(
        name: 'complex functions',
        tree: Bitwise.not(
          Math.max(
            ValueNode(16),
            Math.pow(
              ValueNode(3),
              ValueNode(4),
            ),
          ),
        ),
        expectedValue: -82,
        expectedExpressionString: "~max(16, pow(3, 4))",
      ),
    ];

    testCases.forEach((tc) {
      test(tc.name, () {
        expect(tc.tree.value, equals(tc.expectedValue));
        expect(tc.tree.toString(), equals(tc.expectedExpressionString));
      });
    });
  });

  group("TreeAlgorithms", () {
    test("isDescendant", () {
      Node x = ValueNode(16);
      Node y = ValueNode(25);
      Node root = Select.left(
        Math.negate(x),
        Math.add(
          y,
          ValueNode(2),
        ),
      );
      expect(x.isDescendantOf(y), equals(false));
      expect(y.isDescendantOf(x), equals(false));
      expect(x.isDescendantOf(root), equals(true));
      expect(y.isDescendantOf(root), equals(true));
    });

    group("randomSelectChild", () {
      final String bigExpr = bigTree.toString();
      print(bigExpr);

      for (var i = 0; i < 10; i++) {
        var s = bigTree.randomSelect();
        test(s.toString(), () {
          expect(bigExpr, contains(s.toString()));
        });
      }
    });
  });

  group("tests for swapping", () {
    group("swapValues", () {
      test('happy', () {
        ValueNode x = ValueNode(16);
        ValueNode y = ValueNode(25);

        Node root = Select.left(x, y);
        expect(root.value, equals(16));

        swapValues(root.children[0], root.children[1]);
        expect(root.value, equals(25));
      });
    });

    group("swapOperations", () {
      test("happy", () {
        OperatorNode x = Math.add(
          ValueNode(3),
          ValueNode(16),
        );
        OperatorNode y = Math.pow(
          ValueNode(2),
          ValueNode(25),
        );

        Node root = Math.add(
          Bitwise.not(x),
          Math.multiply(
            y,
            ValueNode(7),
          ),
        );

        expect(root.toString(), equals("(~(3 + 16)) + (pow(2, 25) * 7)"));

        swapOperations(x, y);
        expect(root.toString(), equals("(~pow(3, 16)) + ((2 + 25) * 7)"));
      });
    });

    group("swapStructure", () {
      test("happy", () {
        OperatorNode x = Math.add(
          ValueNode(3),
          ValueNode(16),
        );
        OperatorNode y = Math.pow(
          ValueNode(2),
          ValueNode(25),
        );

        Node root = Math.add(
          Bitwise.not(x),
          Math.multiply(
            y,
            ValueNode(7),
          ),
        );

        expect(root.toString(), equals("(~(3 + 16)) + (pow(2, 25) * 7)"));

        swapStructure(x, y);
        expect(root.toString(), equals("(~pow(2, 25)) + ((3 + 16) * 7)"));
      });
    });

    test("sad", () {
      OperatorNode x = Math.add(
        ValueNode(3),
        ValueNode(16),
      );
      OperatorNode y = Math.pow(
        ValueNode(2),
        x,
      );
      Node root = Math.add(
        ValueNode(25),
        Math.multiply(
          y,
          ValueNode(7),
        ),
      );

      expect(
          () => swapStructure(x, y),
          throwsA(
            "Cannot structurally swap Nodes that are in the same path to the root",
          ));
    });
  });

  group("tests for shouldParenthesize", () {
    const List<String> falseCases = [
      "0",
      "1234",
      "12.34",
      "(10 + 1)",
      "(9 / 6)",
      "(1 / ((10 + 1) * 9))",
      "func(9)",
      "func(10 + 1)",
      "(-func(10 + 1))",
    ];

    const List<String> trueCases = [
      "10 + 1",
      "9 / 6",
      "(10 + 2) + 1",
      "1 + (10 + 2)",
      "-(10 + 1)",
      "-func(9)",
      "-func(10 + 1)",
      "1 / ((10 + 1) * 9)",
    ];

    [
      ...falseCases
          .map(
            (String s) => Tuple2(s, s),
          )
          .toList(),
      ...trueCases
          .map(
            (String s) => Tuple2(s, "($s)"),
          )
          .toList(),
    ].forEach((c) {
      test(c.item1, () {
        expect(c.item1.asUnaryExpression(), equals(c.item2));
      });
    });
  });
}
