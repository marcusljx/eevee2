import 'package:eevee2/extensions/list.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:meta/meta.dart';

class TestCase<T> {
  List<Comparable> input;
  List<Comparable> expected;

  TestCase({@required this.input, @required this.expected});
}

void main() {
  group("tests for extensions/list.dart", () {
    final List<TestCase> testcases = [
      TestCase(input: [], expected: []),
      TestCase(input: [1], expected: [1]),
      TestCase(input: [1, 2, 3, 4], expected: [1, 2, 3, 4]),
      TestCase(input: [1, 2, 3, 2, 4], expected: [1, 2, 4]),
      TestCase(input: [1, 2, 3, 2, 4, 3, 5, 2, 9], expected: [1, 2, 9]),
    ];

    testcases.forEach((testcase) {
      test(testcase.input.toString(), () {
        expect(testcase.input.convergeCyclicPath(), equals(testcase.expected));
      });
    });
  });
}
