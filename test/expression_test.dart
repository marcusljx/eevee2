import 'package:eevee2/ds/expression_tree/expression_tree.dart';
import 'package:eevee2/experiments/best_fit_expression.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  group("buildRandomBinaryExpressionTree", () {
    List<int> values = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

    for (var i = 0; i < 100; i++) {
      test("$i", () {
        Node<num> root = buildRandomBinaryExpressionTree(
          operations: {
            Math.add,
            Math.subtract,
            Math.multiply,
            Math.divide,
          },
          values: values,
        );

        String expr = root.toString();
        values.forEach((num v) {
          expect(expr, contains("$v"));
        });

        print("$expr = ${root.value}");
      });
    }
  });
}
