import 'dart:math';

import 'package:eevee2/eevee2.dart';
import 'package:eevee2/extensions/list.dart';
import 'package:meta/meta.dart';
import 'package:tuple/tuple.dart';

/// Coordinate class.
/// This is effectively the same as a [Tuple2<T extends num, T extends num>] type
class Coordinate<T extends num> implements Comparable<Coordinate<T>> {
  Tuple2<T, T> _point;
  T get x => _point.item1;
  T get y => _point.item2;

  Coordinate(T x, T y) : _point = Tuple2(x, y);

  bool isAdjacentTo(Coordinate<T> other) {
    if (this.x == other.x) {
      return (this.y - other.y).abs() == 1;
    }
    if (this.y == other.y) {
      return (this.x - other.x).abs() == 1;
    }

    return false;
  }

  @override
  int compareTo(Coordinate<T> other) {
    return (this._point == other._point) ? 1 : 0;
  }
}

/// A 2D matrix type for simplified representation of an obstacle map
class Matrix2D<T> {
  List<List<T>> _matrix;
  int get numRows => _matrix.length;
  int get numCols => _matrix.length > 0 ? _matrix[0].length : 0;

  Matrix2D.from(List<List<T>> input) : _matrix = input;

  Matrix2D.generate(int rows, int columns,
      {T Function(int row, int column) fill})
      : _matrix = List.generate(
          rows,
          (rowIndex) => List.generate(
            columns,
            (colIndex) => fill != null ? fill(rowIndex, colIndex) : null,
          ),
        );

  T get(int row, int col) {
    return _matrix[row][col];
  }

  void set(int row, int col, T value) {
    _matrix[row][col] = value;
  }

  @override
  String toString() {
    return _matrix.join("\n");
  }
}

/// The Experimental Solution class
class Pathfinder2DSolution implements Solution<List<Coordinate<int>>> {
  Random rng;
  List<Coordinate<int>> _path;

  /// Map of walkable areas
  final Matrix2D<bool> map;
  final Coordinate<int> source;
  final Coordinate<int> sink;

  Pathfinder2DSolution.random({
    @required this.map,
    @required this.source,
    @required this.sink,
    @required this.rng,
  }) : _path = _generateRandomPath(source, sink, map.numRows, map.numCols);

  Pathfinder2DSolution.from({
    @required List<Coordinate<int>> path,
    @required this.map,
    @required this.source,
    @required this.sink,
    @required this.rng,
  }) : _path = path;

  @override
  Solution<List<Coordinate<int>>> clone() {
    return Pathfinder2DSolution.from(
      path: _path,
      map: map,
      source: source,
      sink: sink,
      rng: rng,
    );
  }

  @override
  Solution<List<Coordinate<int>>> crossWith(
    Solution<List<Coordinate<int>>> other,
  ) {
    // TODO: implement crossWith
    throw UnimplementedError();
  }

  @override
  void mutate() {
    // TODO: implement mutate
  }

  @override
  // TODO: implement score
  double get score => throw UnimplementedError();

  @override
  List<Coordinate<int>> get value => _path;
}

/// Generates a random path from [start] to [end] by
/// picking an intermediary point and recursively building a path from
/// [start] to it, joined with a path from it to [end].
List<Coordinate<int>> _generateRandomPath(
  Coordinate<int> start,
  Coordinate<int> end,
  int maxX,
  int maxY, {
  Random rng,
}) {
  // if adjacent, stop
  if (start.isAdjacentTo(end)) {
    return [start, end];
  }

  rng = rng ?? new Random();
  Coordinate<int> intermediaryPoint = rng.nextBool()

      /// somewhere logically between [start,end]
      ? Coordinate(
          min(start.x, end.x) + rng.nextInt((start.x - end.x).abs()),
          min(start.y, end.y) + rng.nextInt((start.y - end.y).abs()),
        )

      /// random point on the board
      : Coordinate(
          rng.nextInt(maxX),
          rng.nextInt(maxY),
        );

  var pathA = _generateRandomPath(
    start,
    intermediaryPoint,
    maxX,
    maxY,
  ).convergeCyclicPath();

  var pathB = _generateRandomPath(
    intermediaryPoint,
    end,
    maxX,
    maxY,
  ).convergeCyclicPath();

  return pathA..addAll(pathB.sublist(1));
}
