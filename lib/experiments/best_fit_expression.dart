import 'dart:math';

import 'package:eevee2/ds/expression_tree/expression_tree.dart';
import 'package:eevee2/eevee2.dart';
import 'package:meta/meta.dart';

/// Given:
///  - a list of values V
///  - a set of operator symbols S
///  - a value k
///
/// Find an expression consisting of elements from V and S that evaluates to k,
/// where each element in V is used exactly once.

class ExpressionSolution implements Solution<Node<num>> {
  final num targetValue;
  Node<num> _root;
  Random rng;
  List<num> variables;
  Set<BinaryTreeOperatorNodeFactory> operations;

  ExpressionSolution({
    @required this.targetValue,
    @required this.variables,
    @required this.operations,
    this.rng,
    @required Node<num> root,
  }) : _root = root;

  /// Constructor that builds a new randomized ExpressionTree with the given
  /// variables and operations.
  ExpressionSolution.random({
    @required this.targetValue,
    @required this.variables,
    @required this.operations,
    this.rng,
  }) {
    _root = buildRandomBinaryExpressionTree(
      operations: operations,
      values: variables,
      rng: rng,
    );
  }

  @override
  Solution<Node<num>> clone() {
    return ExpressionSolution(
      targetValue: this.targetValue,
      rng: this.rng,
      variables: this.variables,
      operations: this.operations,
      root: this._root,
    );
  }

  @override
  Solution<Node<num>> crossWith(Solution<Node<num>> other) {
    // TODO: implement crossWith
    throw UnimplementedError();
  }

  @override
  void mutate() {
    Node randomOperatorChild;
    while (randomOperatorChild is! OperatorNode) {
      randomOperatorChild = this._root.randomSelect(
            rng: this.rng,
            includeSelf: true,
          );
    }
    var newOperation = this.operations.random(this.rng)(null, null);
    swapOperations(randomOperatorChild as OperatorNode, newOperation);
  }

  @override
  double get score => 1 / ((this.targetValue - this._root.value).abs());

  @override
  Node<num> get value => this._root;
}

Node<num> buildRandomBinaryExpressionTree({
  @required Set<BinaryTreeOperatorNodeFactory> operations,
  @required List<num> values,
  Random rng,
}) {
  List<Node<num>> nodes =
      values.map<Node<num>>((num v) => ValueNode(v)).toList();
  while (nodes.length > 1) {
    nodes.add(
      operations.random(rng)(
        nodes.randomRemove(rng),
        nodes.randomRemove(rng),
      ),
    );
  }
  return nodes[0];
}

class ExpressionExperiment extends Experiment {
  final List<num> variables;
  final Set<OperatorNode> operators;
  final num targetValue;

  ExpressionExperiment({
    this.variables,
    this.operators,
    this.targetValue,
  });
}
