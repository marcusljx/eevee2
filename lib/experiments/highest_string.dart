import 'dart:math';

import 'package:eevee2/eevee2.dart';
import 'package:eevee2/strategy/crossover.dart';
import 'package:meta/meta.dart';

class HighestStringSolution implements Solution<String> {
  Random _rng;
  List<int> _charCodes;
  CrossoverStrategy crossoverStrategy = CrossoverStrategies.randomStrategy([
    CrossoverStrategies.midPointSwap(),
    CrossoverStrategies.nIntervalSwapBuilder(1),
    CrossoverStrategies.nIntervalSwapBuilder(2),
    CrossoverStrategies.nIntervalSwapBuilder(3),
    CrossoverStrategies.nIntervalSwapBuilder(4),
  ]);

  HighestStringSolution.random({
    @required int length,
    @required Random rng,
  })  : this._rng = rng,
        this._charCodes = List.generate(length, (_) => _randomCharCode(rng));

  HighestStringSolution._fromCharCodes(this._charCodes, this._rng);

  @override
  Solution<String> clone() {
    return HighestStringSolution._fromCharCodes(_charCodes, _rng);
  }

  @override
  double get score => _charCodes.fold(0, (prev, curr) => prev + curr);

  @override
  String get value => this.toString();

  @override
  String toString() {
    return String.fromCharCodes(_charCodes);
  }

  @override
  void mutate() {
    _charCodes[_rng.nextInt(_charCodes.length)] = _randomCharCode(_rng);
  }

  @override
  HighestStringSolution crossWith(Solution<String> other) {
    if (other is! HighestStringSolution) {
      throw Exception(
          "mixed Solution<String> types: expected ${this.runtimeType} but got ${other.runtimeType}");
    }

    var otherCodes = (other as HighestStringSolution)._charCodes;

    return HighestStringSolution._fromCharCodes(
      crossoverStrategy(
        this._charCodes,
        otherCodes,
      ).map((e) => e as int).toList(),
      _rng,
    );
  }

  static int _randomCharCode(Random rng) {
    return 'a'.codeUnitAt(0) + rng.nextInt(26);
  }
}

void main() {
  final rng = new Random();
  const stringLength = 10;

  Experiment<String> experiment = new Experiment(
    maxGenerations: 200,
    mutationProbability: 0.15,
    maxHighScoreIterations: 50,
    maxScore: 1220,
    rng: rng,
    populationGenesisFunc: () {
      return List<HighestStringSolution>.generate(
        30,
        (_) => HighestStringSolution.random(
          length: stringLength,
          rng: rng,
        ),
      );
    },
  );

  /// run experiment
  experiment.execute();
}
