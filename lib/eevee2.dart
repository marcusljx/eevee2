library eevee2;

import 'dart:math';

import 'package:eevee2/strategy/pairwise_selection.dart';
import 'package:eevee2/strategy/population_selection.dart';
import 'package:meta/meta.dart';
import 'package:tuple/tuple.dart';

class Experiment<T> {
  /// The internal random number generator used
  Random rng;

  /// Generation number
  int get generation => _generation;
  int _generation = 0;
  String get generationString => this
      .generation
      .toString()
      .padLeft(this.maxGenerations.toString().length, '0');

  /// The population held by the experiment
  List<Solution<T>> get population => _population;
  List<Solution<T>> _population;

  /// The maximum number of generations to iterate
  final int maxGenerations;

  /// If specified, the experiment will terminate prematurely as long as
  /// 1 solution in the population has a score >= [maxScore].
  ///
  /// Even if the original population (from [populationGenesisFunc]) contains
  /// a solution that has a score above this value,
  /// the experiment will still iterate at least once.
  final double maxScore;

  /// If specified, terminates the experiment once a high-score has remained the
  /// highest-score for [maxHighScoreIterations],
  /// regardless of the solution which has the score.
  final int maxHighScoreIterations;

  /// The probability of performing crossover.
  /// This must be in the range [0.0, 1.0].
  final double crossoverProbability;

  /// The probability of performing mutation.
  /// This must be in the range [0.0, 1.0].
  final double mutationProbability;

  /// Selection strategy for each next generation
  final PopulationSelectionStrategy<Solution> populationSelectionStrategy;

  /// Selection strategy for coupling of solutions (eg. for crossover)
  final PairwiseSelectionStrategy<Solution> pairwiseSelectionStrategy;

  Experiment({
    @required List<Solution<T>> Function() populationGenesisFunc,
    @required this.maxGenerations,
    this.mutationProbability = 0.03,
    this.crossoverProbability = 1.0,
    this.populationSelectionStrategy =
        PopulationSelectionStrategies.highestScore,
    this.pairwiseSelectionStrategy =
        PairwiseSelectionStrategies.orderedPairwise,
    Random rng,
    this.maxScore,
    this.maxHighScoreIterations,
  })  : assert(maxGenerations >= 0),
        assert(mutationProbability >= 0.0 && mutationProbability <= 1.0),
        assert(crossoverProbability >= 0.0 && crossoverProbability <= 1.0),
        assert(populationSelectionStrategy != null),
        assert(pairwiseSelectionStrategy != null),
        this.rng = rng ?? new Random(),
        this._population = populationGenesisFunc() ?? [];

  /// Retrieve the top-scoring solution in the current generation
  Solution<T> top() {
    return this._population.reduce(
        (value, element) => element.score > value.score ? element : value);
  }

  /// Retrieve the bottom-scoring solution in the current generation
  Solution<T> bottom() {
    return this._population.reduce(
        (value, element) => element.score < value.score ? element : value);
  }

  String toStringShort() {
    Solution<T> t = top();
    Solution<T> b = bottom();
    return "Generation #${this.generation.toString().padLeft(this.maxGenerations.toString().length, '0')}\t ${b.value} (${b.score})\t <->\t ${t.value} (${t.score})";
  }

  @override
  String toString() {
    return """Generation #${this.generationString}
${this._population.map((p) => "${p.value}  =>  ${p.score}").join("\n")}
=======================================================
""";
  }

  /// Runs the generation process exactly once
  void iterate() {
    List<Solution<T>> newSolutions = new List();

    /// perform crossover
    if (this.crossoverProbability > this.rng.nextDouble()) {
      this
          .pairwiseSelectionStrategy(this.population)
          .forEach((Tuple2<Solution, Solution> pair) {
        newSolutions.addAll([
          pair.item1.crossWith(pair.item2),
          pair.item2.crossWith(pair.item1),
        ]);
      });
    }

    /// add old generation
    newSolutions.addAll(this.population);

    /// mutate all solutions
    newSolutions.forEach((Solution<T> solution) {
      if (this.mutationProbability > this.rng.nextDouble()) {
        solution.mutate();
      }
    });

    /// select next generation
    this._population =
        this.populationSelectionStrategy(newSolutions, population.length);

    /// increment generation number
    this._generation++;
  }

  void execute() {
    double highScore = double.negativeInfinity;
    int highScoreIterations = 0;
    TerminationReason reason = TerminationReason.MaxGenerationsReached;

    for (; this._generation < maxGenerations; this._generation++) {
      this.iterate();

      /// break if reached maxScore
      Solution top = this.top();
      if (this.maxScore != null) {
        if (top.score >= this.maxScore) {
          reason = TerminationReason.MaxScoreReached;
          break;
        }
      }

      /// break if high-score has been consistent over [maxHighScoreIterations]
      if (this.maxHighScoreIterations != null) {
        highScoreIterations =
            (top.score == highScore) ? highScoreIterations + 1 : 1;
        highScore = top.score;

        if (highScoreIterations >= this.maxHighScoreIterations) {
          reason = TerminationReason.MaxHighScoreIterationsReached;
          break;
        }
      }

      print(this.toStringShort());
    }

    print(this.toString());
    print(
        "Experiment stopped at Generation #${this.generationString} with reason: $reason");
  }
}

/// Abstract class representing and individual experiment solutions.
/// [T] represents the value type of the [Solution],
/// and [C] represents the class itself.
abstract class Solution<T> {
  /// Score of the current solution
  double get score;

  /// Value representation of the current solution
  T get value;

  /// Method of cloning the solution
  Solution<T> clone();

  /// Mutation method
  void mutate();

  /// Crossover with [other].
  /// Crossovers have directionality,
  /// so other.crossWith(this) gives a different result
  Solution<T> crossWith(Solution<T> other);
}

/// The reason set for termination of an experiment
enum TerminationReason {
  MaxGenerationsReached,
  MaxHighScoreIterationsReached,
  MaxScoreReached,
}

extension RandomSelectionUtils<E> on Iterable<E> {
  E random([Random rng]) {
    rng ??= Random();

    return this.elementAt(rng.nextInt(this.length));
  }
}

extension RandomListSelectionUtils<E> on List<E> {
  E randomRemove([Random rng]) {
    rng ??= Random();
    int i = rng.nextInt(this.length);
    return this.removeAt(i);
  }
}
