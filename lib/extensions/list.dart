extension mutations on List<Comparable> {
  List convergeCyclicPath() {
    int maxI = 0;
    int maxJ = 0;
    int maxGap = 0;

    this.asMap().forEach((int i, value) {
      for (int j = i + 1; j < this.length; j++) {
        if ((this[j] == value) && (j - i > maxGap)) {
          maxI = i;
          maxJ = j;
          maxGap = j - i;
        }
      }
    });

    if (maxGap > 0) {
      return new List.from(this)..removeRange(maxI, maxJ);
    } else {
      return new List.from(this);
    }
  }
}
