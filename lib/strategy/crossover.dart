typedef CrossoverStrategy<E> = List<E> Function(List<E> a, List<E> b);

/// A collection of Crossover Strategies.
///
/// These methods return List<dynamic>,
/// which typically should be recasted to List<E> using the following:
///
///   var result = CrossoverStrategies.midPointSwap(a, b);
///   return result.map((e) => e as int).toList();
class CrossoverStrategies {
  // This class is not meant to be instatiated or extended; this constructor
  // prevents instantiation and extension.
  // ignore: unused_element
  CrossoverStrategies._();

  static CrossoverStrategy nIntervalSwapBuilder(int interval) {
    return (List a, List b) => _nIntervalSwap(a, b, interval);
  }

  static CrossoverStrategy midPointSwap() {
    return (List a, List b) => _midPointSwap(a, b);
  }

  static CrossoverStrategy randomStrategy(List<CrossoverStrategy> strategies) {
    assert(strategies.length > 0);
    return (List a, List b) {
      strategies.shuffle();
      return strategies[0](a, b);
    };
  }
}

/// Returns a joined list front half of [a] and back half of [b]
List _midPointSwap(List a, List b) {
  assert(a.length == b.length);
  if ((a == b) || (a.length == 1)) {
    return a;
  }
  return List.generate(
    a.length,
    (index) => index < a.length / 2 ? a[index] : b[index],
  );
}

List _nIntervalSwap(List a, List b, int interval) {
  assert(a.length == b.length);
  assert(interval >= 1);

  if ((a == b) || (a.length == 1)) {
    return a;
  }

  return List.generate(
    a.length,
    (index) => (index / interval) % 2 == 0 ? a[index] : b[index],
  );
}

/*
  n = 2

* 0 true    0/2 = 0
* 1 true    1/2 = 0
* 2 false   2/2 = 1
* 3 false   3/2 = 1
* 4 true    4/2 = 2
* 5 true    5/2 = 2
* 6 false   6/2 = 3
* 7 false   7/2 = 3
* 8 true    8/2 = 4
* 9 true    9/2 = 4
*
*
* */
