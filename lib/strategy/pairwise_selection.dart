import 'package:eevee2/eevee2.dart';
import 'package:tuple/tuple.dart';

/// Selection strategy for pairing solutions from a list of solutions.
/// This is used to "matchmake" solutions for crossover.
typedef PairwiseSelectionStrategy<T> = List<Tuple2<T, T>> Function(
    List<T> solutions);

/// A collection of static methods of [PairwiseSelectionStrategy] type
class PairwiseSelectionStrategies {
  static const PairwiseSelectionStrategy<Solution> orderedPairwise =
      _orderedPairwise;
  static const PairwiseSelectionStrategy<Solution> foldInPairwise =
      _foldInPairwise;
}

/// Selects in order, 0-1, 2-3, 4-5, etc.
/// If [solutions.length] is odd, the final element is not included.
List<Tuple2<Solution, Solution>> _orderedPairwise(List<Solution> solutions) {
  List<Tuple2<Solution, Solution>> result = new List();
  for (int i = 0; i < solutions.length; i += 2) {
    result.add(Tuple2(solutions[i], solutions[i + 1]));
  }
  return result;
}

/// Selects first and last in order, indices 0 with n-1, 1 with n-2, etc.
List<Tuple2<Solution, Solution>> _foldInPairwise(List solutions) {
  List<Tuple2> result = new List();
  for (int i = 0; i < solutions.length / 2; i++) {
    result.add(Tuple2(solutions[i], solutions[solutions.length - (i + 1)]));
  }
  return result;
}
