import 'dart:math';

import 'package:eevee2/eevee2.dart';

/// Selection strategy for retrieving the next generation from
/// an older list of [solutions].
typedef PopulationSelectionStrategy<T> = List<T> Function(
    List<T> solutions, int n);

/// A collection of static methods of [PopulationSelectionStrategy] type
class PopulationSelectionStrategies {
  /// Randomly selects [n] results from [solutions]
  static List<Solution> randomSelect(List<Solution> solutions, int n) {
    solutions.shuffle(new Random());
    return solutions.sublist(solutions.length - n);
  }

  /// Returns the [n] top scorers
  static List<Solution> highestScore(List<Solution> solutions, int n) {
    solutions.sort((Solution a, Solution b) {
      var res = a.score - b.score;
      return res == 0 ? 0 : (res < 0 ? -1 : 1);
    });

    return solutions.sublist(solutions.length - n);
  }

  /// Returns a 90-10 population of high-low scorers
  static List<Solution> highWithLowP10(List<Solution> solutions, int n) {
    solutions.sort((Solution a, Solution b) {
      var res = a.score - b.score;
      return res == 0 ? 0 : (res < 0 ? -1 : 1);
    });

    List<Solution> result;
    // fill low population
    int i = 0;
    for (; i < n / 10; i++) {
      result.add(solutions[i]);
    }

    // fill high population
    result.addAll(solutions.sublist(solutions.length - (n + i)));
    return result;
  }
}
