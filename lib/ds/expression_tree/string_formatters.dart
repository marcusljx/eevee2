part of 'expression_tree.dart';

/// Signature of a function that returns the
/// string representation of the operation on [args].
typedef ExpressionFormatter = String Function(List<String> childrenExpressions);

/// Wraps all [values] as variables with the given operation [affix],
/// which should be a symbol of some sort.
ExpressionFormatter infix(String affix) {
  return (List<String> values) {
    return values
        .map(
          (v) => v.asUnaryExpression(),
        )
        .join(" $affix ");
  };
}

/// Wraps all [values] as arguments to a function with the given [name].
ExpressionFormatter function(String name) {
  return (List<String> values) => "$name(${values.map(
        (v) => v.asUnaryExpression(),
      ).join(", ")})";
}

/// prefixes only the first value with an affix.
/// If [values] does not contain exactly 1 element,
/// this function throws.
ExpressionFormatter prefix(String affix) {
  return (List<String> values) {
    if (values.length != 1) {
      throw "Expected values.length == 1; got ${values.length}";
    }

    String value = values[0];
    return "$affix${value.asUnaryExpression()}";
  };
}

extension Expressions on String {
  String asUnaryExpression() {
    return shouldParenthesizeForPrefix(this) ? "(${this})" : this;
  }
}

bool shouldParenthesizeForPrefix(String s) {
  if (s.length == 0) {
    throw "shouldParenthesizeForPrefix called with empty string";
  }

  bool beginsWithOperatorPrefix =
      RegExp(r'[a-zA-Z0-9(]').matchAsPrefix(s) == null;

  int v = 0;
  for (var c in s.characters) {
    if (c == " " && v == 0) {
      return true;
    }

    if (c == "(") {
      v++;
    }

    if (c == ")") {
      v--;
    }
  }

  if (v != 0) {
    throw "expression contains unclosed parentheses";
  }

  return false || beginsWithOperatorPrefix;
}
