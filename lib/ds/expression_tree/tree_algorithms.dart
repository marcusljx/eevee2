part of 'expression_tree.dart';

extension TreeAlgorithms on Node {
  bool isDescendantOf(Node node) {
    var current = this;
    while (current != null) {
      if (current == node) {
        return true;
      }

      current = current.parent;
    }
    return false;
  }

  Node randomSelect({
    Random rng,
    bool includeSelf = false,
  }) {
    rng ??= Random();
    List<Node> allNodes = [];
    _bfs(this, (node) => allNodes.add(node));

    if (!includeSelf) {
      allNodes = allNodes.sublist(1);
    }

    return allNodes[rng.nextInt(allNodes.length)];
  }
}

void _bfs(Node root, Function(Node) walkFunc) {
  walkFunc(root);
  root.children.forEach((Node child) {
    _bfs(child, walkFunc);
  });
}
