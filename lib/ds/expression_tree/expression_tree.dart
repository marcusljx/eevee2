library expression_tree;

import 'dart:math' as DartMath;
import 'dart:math';

import 'package:characters/characters.dart';
import 'package:meta/meta.dart';

part 'node.dart';
part 'operation.dart';
part 'operations.dart';
part 'string_formatters.dart';
part 'tree_algorithms.dart';

/// Function signature of an Operation
typedef OperatorFunc<T> = T Function(List<T>);
