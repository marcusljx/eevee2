part of "expression_tree.dart";

/// Bitwise Operations acting on [int] ValueNodes only.
///
/// Performing these operations on [Node]s with [double]
/// type will result in a runtime error.
class Bitwise {
  Bitwise._();

  static final UnaryTreeOperatorNodeFactory not = UnaryTreeOperatorNode.factory(
    operation: (x) => ~(x as int),
    expressionFormatter: prefix("~"),
  );

  static final BinaryTreeOperatorNodeFactory and =
      BinaryTreeOperatorNode.factory(
    operation: (a, b) => (a as int) & (b as int),
    expressionFormatter: infix("&"),
  );
  static final BinaryTreeOperatorNodeFactory or =
      BinaryTreeOperatorNode.factory(
    operation: (a, b) => (a as int) | (b as int),
    expressionFormatter: infix("|"),
  );
  static final BinaryTreeOperatorNodeFactory xor =
      BinaryTreeOperatorNode.factory(
    operation: (a, b) => (a as int) ^ (b as int),
    expressionFormatter: infix("^"),
  );
  static final BinaryTreeOperatorNodeFactory shiftLeft =
      BinaryTreeOperatorNode.factory(
    operation: (a, b) => (a as int) << (b as int),
    expressionFormatter: infix("<<"),
  );
  static final BinaryTreeOperatorNodeFactory shiftRight =
      BinaryTreeOperatorNode.factory(
    operation: (a, b) => (a as int) >> (b as int),
    expressionFormatter: infix(">>"),
  );
}

/// Selection operators
class Select {
  /// Ignores the right-side value and only returns the left-side value
  static final BinaryTreeOperatorNodeFactory left =
      BinaryTreeOperatorNode.factory(
    operation: (a, _) => a,
    expressionFormatter: function("left"),
  );

  /// Ignores the left-side value and only returns the right-side value
  static final BinaryTreeOperatorNodeFactory right =
      BinaryTreeOperatorNode.factory(
    operation: (_, b) => b,
    expressionFormatter: function("right"),
  );
}

/// Classic Math Operators
class Math {
  Math._();

  /// Ignores the input and always returns 0
  static final UnaryTreeOperatorNodeFactory zero =
      UnaryTreeOperatorNode.factory(
    operation: (_) => 0,
    expressionFormatter: function("zero"),
  );

  /// Returns the negative of its child value
  static final UnaryTreeOperatorNodeFactory negate =
      UnaryTreeOperatorNode.factory(
    operation: (x) => -x,
    expressionFormatter: prefix("-"),
  );

  static final UnaryTreeOperatorNodeFactory sqrt =
      UnaryTreeOperatorNode.factory(
    operation: DartMath.sqrt,
    expressionFormatter: function("sqrt"),
  );
  static final UnaryTreeOperatorNodeFactory log = UnaryTreeOperatorNode.factory(
    operation: DartMath.log,
  );

  static final BinaryTreeOperatorNodeFactory add =
      BinaryTreeOperatorNode.factory(
    operation: (num a, num b) => a + b,
    expressionFormatter: infix("+"),
  );
  static final BinaryTreeOperatorNodeFactory subtract =
      BinaryTreeOperatorNode.factory(
    operation: (a, b) => a - b,
    expressionFormatter: infix("-"),
  );
  static final BinaryTreeOperatorNodeFactory multiply =
      BinaryTreeOperatorNode.factory(
    operation: (a, b) => a * b,
    expressionFormatter: infix("*"),
  );
  static final BinaryTreeOperatorNodeFactory divide =
      BinaryTreeOperatorNode.factory(
    operation: (a, b) => a / b,
    expressionFormatter: infix("/"),
  );
  static final BinaryTreeOperatorNodeFactory integerDivide =
      BinaryTreeOperatorNode.factory(
    operation: (a, b) => a ~/ b,
    expressionFormatter: infix("~/"),
  );
  static final BinaryTreeOperatorNodeFactory mod =
      BinaryTreeOperatorNode.factory(
    operation: (a, b) => a % b,
    expressionFormatter: infix("%"),
  );

  static final BinaryTreeOperatorNodeFactory pow =
      BinaryTreeOperatorNode.factory(
    operation: DartMath.pow,
    expressionFormatter: function("pow"),
  );
  static final BinaryTreeOperatorNodeFactory max =
      BinaryTreeOperatorNode.factory(
    operation: DartMath.max,
    expressionFormatter: function("max"),
  );
  static final BinaryTreeOperatorNodeFactory min =
      BinaryTreeOperatorNode.factory(
    operation: DartMath.min,
    expressionFormatter: function("min"),
  );
}
