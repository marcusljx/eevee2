part of 'expression_tree.dart';

typedef UnaryTreeOperatorNodeFactory = UnaryTreeOperatorNode Function(
    Node<num>);

class UnaryTreeOperatorNode extends OperatorNode {
  Node child;

  UnaryTreeOperatorNode({
    @required OperatorFunc<num> operatorFunc,
    @required ExpressionFormatter expressionFormatter,
    @required Node<num> child,
  }) : super(
          operatorFunc: operatorFunc,
          expressionFormatter: expressionFormatter,
          children: [child],
        );

  static UnaryTreeOperatorNodeFactory factory({
    Function(num) operation,
    ExpressionFormatter expressionFormatter,
  }) {
    return (Node<num> child) => UnaryTreeOperatorNode(
          operatorFunc: (List<num> args) => operation(args[0]),
          expressionFormatter: expressionFormatter,
          child: child,
        );
  }
}

typedef BinaryTreeOperatorNodeFactory = BinaryTreeOperatorNode Function(
    Node<num>, Node<num>);

class BinaryTreeOperatorNode<T extends num> extends OperatorNode {
  Node<T> left;
  Node<T> right;

  BinaryTreeOperatorNode({
    @required OperatorFunc<num> operatorFunc,
    @required ExpressionFormatter expressionFormatter,
    @required Node<num> left,
    @required Node<num> right,
  }) : super(
          operatorFunc: operatorFunc,
          expressionFormatter: expressionFormatter,
          children: [left, right],
        );

  static BinaryTreeOperatorNodeFactory factory({
    Function(num, num) operation,
    ExpressionFormatter expressionFormatter,
  }) {
    return (Node<num> left, Node<num> right) => BinaryTreeOperatorNode(
          operatorFunc: (List<num> args) => operation(args[0], args[1]),
          expressionFormatter: expressionFormatter,
          left: left,
          right: right,
        );
  }
}
