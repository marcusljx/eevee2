part of 'expression_tree.dart';

/// A Node in the expression tree.
///
/// Each Node has a [value] getter,
/// which recursively resolves the value by
/// calling its children's [value] getter.
abstract class Node<T> {
  T get value;
  List<Node<T>> children;

  Node<T> get parent;
  _setParent(Node<T> node);

  Node<T> clone();
}

/// mixin for a
mixin BidirectionalNodeMixin<T> implements Node<T> {
  T _value; // must override
  T get value => _value;

  /// parent
  Node<T> _parent;
  Node<T> get parent => _parent;
  _setParent(Node<T> node) {
    _parent = node;
  }

  /// children
  List<Node<T>> children = const [];

  /// Returns exactly the String representation of the value it is holding
  @override
  String toString() {
    return "$_value";
  }
}

/// Leaf [Node] type for storing primitive values of extending from [num].
class ValueNode with BidirectionalNodeMixin<num> {
  /// Constructor
  ValueNode(num value) {
    this._value = value;
  }

  @override
  Node<num> clone() {
    return ValueNode(this._value);
  }
}

/// Node representing an operation over an arbitrary number of children [Node]s.
class OperatorNode with BidirectionalNodeMixin<num> {
  /// The function that performs the actual operation represented by the Node
  OperatorFunc<num> operatorFunc;
  ExpressionFormatter expressionFormatter;
  @override

  /// Constructor
  ///
  /// Each [OperatorNode] must be instantiated with an [operatorFunc] and
  /// an [expressionFormatter],
  /// which is used to represent the expression tree as a string.
  OperatorNode({
    @required this.operatorFunc,
    @required this.expressionFormatter,
    List<Node<num>> children = const [],
  }) {
    this._setChildren(children);
  }

  /// Copies the [operatorFunc] and [expressionFormatter]
  /// from the given node, whilst keeping
  OperatorNode.from(
    OperatorNode node, {
    List<Node<num>> children = const [],
  })  : this.operatorFunc = node.operatorFunc,
        this.expressionFormatter = node.expressionFormatter {
    this._setChildren(children);
  }

  void changeOperation({
    @required OperatorFunc<num> operatorFunc,
    @required ExpressionFormatter expressionFormatter,
  }) {}

  // set the children and sets [this] as the parent of each child
  void _setChildren(List<Node<num>> children) {
    this.children = children;
    this.children.forEach((child) => child._setParent(this));
  }

  /// Returns the result of the operator on the
  /// values recursively returned by each child Node
  @override
  num get value => operatorFunc(
        children.map((Node<num> node) => node.value).toList(),
      );

  /// String representation of the current expression
  @override
  String toString() {
    return expressionFormatter(
      children.map<String>((Node node) => node.toString()).toList(),
    );
  }

  @override
  Node<num> clone() {
    return OperatorNode(
      operatorFunc: this.operatorFunc,
      expressionFormatter: this.expressionFormatter,
      children: this.children.map((child) => child.clone()).toList(),
    );
  }
}

/// Directly swaps the values of [a] and [b]
void swapValues(ValueNode a, ValueNode b) {
  var aValue = a._value;
  a._value = b._value;
  b._value = aValue;
}

/// Directly swaps the value or operation representation of [a] and [b]
void swapOperations(OperatorNode a, OperatorNode b) {
  if (a.children.length != b.children.length) {
    throw "Cannot swap operations if OperatorNodes have different number of children : ${a.children.length} != ${b.children.length}";
  }

  var aOp = a.operatorFunc;
  var aExp = a.expressionFormatter;
  a.operatorFunc = b.operatorFunc;
  a.expressionFormatter = b.expressionFormatter;
  b.operatorFunc = aOp;
  b.expressionFormatter = aExp;
}

void swapStructure(Node a, Node b) {
  if (a.isDescendantOf(b) || b.isDescendantOf(a)) {
    throw "Cannot structurally swap Nodes that are in the same path to the root";
  }

  var aParent = a.parent;
  var aIdx = aParent.children.indexOf(a);

  var bParent = b.parent;
  var bIdx = bParent.children.indexOf(b);

  aParent.children[aIdx] = b;
  b._setParent(aParent);

  bParent.children[bIdx] = a;
  a._setParent(bParent);
}
